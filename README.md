# ENSC 474 - Spring 2018
* Author: [Richard Chen](mailto:ryc5@sfu.ca)
* Git Repo: [Bitbucket](https://bitbucket.org/rchen93/ensc_474/)

## Assignment 2
* Custom functions are stored in ```/lib```
* Main script is ```a2/a2.m```
	* ```Bernie1.pgm``` and ```Harbor1.pgm``` must be in the same directory as ```a2.m```
	* The script will output all the processed images in the same directory

## Assignment 3
* Custom functions are stored in ```/lib```
* Main script is ```a3/a3.m```
	* ```1.pgm, 2.pgm, 3.pgm, 4.pgm and bernieEnemy.pgm``` must be in the same directory as ```a3.m```
	* The script will output all the processed images in the same directory
	* Setting scale_factor in ```a3.m``` will scale all output images using the bilinear method

## Assignment 4
* Custom functions are stored in ```/lib```
* Main script is ```a4/a4.m```
	* ```parrot.pgm``` must be in the same directory as ```a4.m```
	* The script will output all the processed images in the same directory

## Assignment 5
* Custom functions are stored in ```/lib```
* Main script is ```a5/a5.m```
	* ```finger1.pgm, finger2.pgm, license.pgm, and NoisyImg.pgm``` must be in the same directory as ```a5.m```
	* The script will output all the processed images in the same directory

## Assignment 6
* Custom functions are stored in ```/lib```
* Main script is ```a6/a6.m```
	* ```MotionBlurred.pgm, Lenna.pgm, and CircleSquare.pgm``` must be in the same directory as ```a6.m```
	* The script will output all the processed images in the same directory