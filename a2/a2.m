% Assignment 2
% ENSC 474 - Spring 2018
% 
% Richard Chen
% ryc5@sfu.ca
% 301171618

%% Setup
clear
clc

% Add path to custom functions
addpath ../lib

%% Question 1a
% Reduce the resolution of an image by factors of 2 and 4, 8, 16 and 32. 
% Use the deleting scheme in this case.
file_in = fread_pgm('Bernie1.pgm');

for factor = [2, 4, 8, 16, 32]
    file_out = reduce_del(file_in, factor);
    fwrite_pgm(['Bernie_del_' num2str(factor) '.pgm'], file_out)
end

%% Question 1b
% Reduce the resolution of an image by factors of 2 and 4, 8, 16 and 32. 
% Use the mean value substitution in this part.
file_in = fread_pgm('Bernie1.pgm');

for factor = [2, 4, 8, 16, 32]
    file_out = reduce_avg(file_in, factor);
    fwrite_pgm(['Bernie_avg_' num2str(factor) '.pgm'], file_out)
end

%% Question 1c
% Reduce the number of gray levels in an image by factors of 2, 4, 8, 16 and 32.
file_in = fread_pgm('Bernie1.pgm');

for factor = [2, 4, 8, 16, 32]
    file_out = reduce_grey_levels(file_in, factor);
    fwrite_pgm(['Bernie_grey_lvl_' num2str(factor) '.pgm'], file_out)
end

%% Question 2
% Reduce the resolution of an image by factors of 1.5. 
% Use a bilinear interpolation scheme in this part.
file_in = fread_pgm('Harbor1.pgm');

for factor = 1.5
    file_out = reduce_bilinear(file_in, factor);
    fwrite_pgm(['Harbor_bilinear_' num2str(factor) '.pgm'], file_out)
end