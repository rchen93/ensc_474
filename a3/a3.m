% Assignment 3
% ENSC 474 - Spring 2018
% 
% Richard Chen
% ryc5@sfu.ca
% 301171618

%% Setup
clear
clc

% Add path to custom functions
addpath ../lib

% Output file scaling factor
scale_factor = 1.0;

%% Question 1
% Apply weighted averaging filters to the following images
% 
%   Files  | Filters
%  ------------------------
%  '1.pgm' | a b c d e h i
%  '2.pgm' | a e f g
%  '3.pgm' | f g
%  '4.pgm' | a e f h i

% User-defined filter cells
filter = containers.Map;
filter('a') = 1/3^2 * ones(3);
filter('b') = 1/5^2 * ones(5);
filter('c') = 1/7^2 * ones(7);
filter('d') = 1/11^2 * ones(11);
filter('e') = [1/16 2/16 1/16; 2/16 4/16 2/16; 1/16 2/16 1/16];
filter('f') = [0 -1 0; -1 5 -1; 0 -1 0];
filter('g') = [-1 -1 -1; -1 9 -1; -1 -1 -1];
filter('h') = [-1 0 1; -2 0 2; -1 0 1];

% Filters applied to each file
file{1} = ['a' 'b' 'c' 'd' 'e' 'h' 'i'];
file{2} = ['a' 'e' 'f' 'g'];
file{3} = ['f' 'g'];
file{4} = ['a' 'e' 'f' 'h' 'i'];

for f = 1:length(file)
    % Open file
    file_in = fread_pgm([num2str(f) '.pgm']);
    
    % Apply filters
    for key = file{f}
        if key == 'i'
            file_out_name = [num2str(f) '_' key '.pgm'];
            disp(file_out_name)
            
            % Median filter of 5x5
            tic
            file_out = filter_median(file_in, 5);
            toc
            
            % Output
            file_out = scale_bilinear(file_out, scale_factor);
            fwrite_pgm(file_out_name, file_out);
        else
            file_out_name = [num2str(f) '_' key '.pgm'];
            disp(file_out_name)
            
            % Weighted average filter
            tic
            file_out = filter_weighted_avg(file_in, filter(key));
            toc
            
            % Output
            file_out = scale_bilinear(file_out, scale_factor);
            fwrite_pgm(file_out_name, file_out);
        end
    end
end

%% Question 2
% Global histogram equalization
file_in = fread_pgm('bernieEnemy.pgm');
file_out = histogram_equalization(file_in);
file_out = scale_bilinear(file_out, scale_factor);
fwrite_pgm('bernieEnemy_equal.pgm', file_out)