% Assignment 4
% ENSC 474 - Spring 2018
% 
% Richard Chen
% ryc5@sfu.ca
% 301171618

%% Setup
clear
clc

% Add path to custom functions
addpath ../lib

%% Question 1
% 1. Take the 2-D FFT of the image (using Matlab's pre-written function)
% 2. In the Fourier domain, remove the phase (set the phase to zero)
% 3. Take the inverse 2-D FFT of the image

file_in = fread_pgm('parrot.pgm');

% Create padding of 2M x 2N
mat = zeros(2 * file_in.height, 2 * file_in.width);

% Convert vector to matrix
mat(1:file_in.height, 1:file_in.width) = reshape(file_in.data, file_in.width, file_in.height)';

% Center the transform
[row, col] = size(mat);
for x = 1:row
    for y = 1:col
        mat(x, y) = mat(x, y) * (-1) ^ (x + y);
    end
end

% 2-D FFT
mat = fft2(mat);

% Remove phase angle (keep magnitude only)
mat = real(mat);

% Inverse 2-D FFT
mat = real(ifft2(mat));

% Center the transform
[row, col] = size(mat);
for x = 1:row
    for y = 1:col
        mat(x, y) = mat(x, y) * (-1) ^ (x + y);
    end
end

% Remove padding
mat = mat(1:file_in.height, 1:file_in.width);

% Convert matrix to vector
mat = reshape(mat', [], 1);

% Output file
file_out = file_in;
file_out.data = mat;
fwrite_pgm('1-real.pgm', file_out);

%% Question 2
% Perform a low pass filtering on the Parrot image by leaving all Fourier 
% coefficients that fall within the cutoff frequency of the low pass 
% filter untouched and setting coefficients that fall outside to zero.

file_in = fread_pgm('parrot.pgm');

% Create padding of 2M x 2N
mat = zeros(2 * file_in.height, 2 * file_in.width);

% Convert vector to matrix
mat(1:file_in.height, 1:file_in.width) = reshape(file_in.data, file_in.width, file_in.height)';

% Center the transform
[row, col] = size(mat);
for x = 1:row
    for y = 1:col
        mat(x, y) = mat(x, y) * (-1) ^ (x + y);
    end
end

% 2-D FFT
mat = fft2(mat);

% Create 3 different ideal low pass filters
radius = [90 120 150];

for r = 1:length(radius)
    filt{r} = zeros(2 * file_in.height, 2 * file_in.width);
    [row, col] = size(mat);
    center = [row/2 col/2];

    for u = 1:row
        for v = 1:col
            if (center(1) - u)^2 + (center(2) - v)^2 <= radius(r)^2
                filt{r}(u, v) = 1;
            end
        end
    end
    
    % Apply filter
    filtered = mat .* filt{r};

    % Inverse 2-D FFT
    filtered = real(ifft2(filtered));

    % Center the transform
    [row, col] = size(filtered);
    for x = 1:row
        for y = 1:col
            filtered(x, y) = filtered(x, y) * (-1) ^ (x + y);
        end
    end

    % Remove padding
    filtered = filtered(1:file_in.height, 1:file_in.width);

    % Convert matrix to vector
    filtered = reshape(filtered', [], 1);

    % Output file
    file_out = file_in;
    file_out.data = filtered;
    fwrite_pgm(['2-low_pass_' num2str(radius(r)) '.pgm'], file_out);
end

%% Question 3
% Use Laplacian in the frequency domain to high-boost filter the image

file_in = fread_pgm('parrot.pgm');

% Create padding of 2M x 2N
mat = zeros(2 * file_in.height, 2 * file_in.width);

% Convert vector to matrix
mat(1:file_in.height, 1:file_in.width) = reshape(file_in.data, file_in.width, file_in.height)';

% Center the transform
[row, col] = size(mat);
for x = 1:row
    for y = 1:col
        mat(x, y) = mat(x, y) * (-1) ^ (x + y);
    end
end

% 2-D FFT
mat = fft2(mat);

% Create Laplacian filter
filt = zeros(2 * file_in.height, 2 * file_in.width);
[row, col] = size(mat);
center = [row/2 col/2];

for u = 1:row
    for v = 1:col
        filt(u, v) = ((center(1) - u)^2 + (center(2) - v)^2);
    end
end

filt = (filt - min(filt(:))) / (max(filt(:)) - min(filt(:))) * 255;
filt = (2.1 - 1) + 0.02 * filt;

% Apply filter
mat = mat .* filt;

% Inverse 2-D FFT
mat = real(ifft2(mat));

% Center the transform
[row, col] = size(mat);
for x = 1:row
    for y = 1:col
        mat(x, y) = mat(x, y) * (-1) ^ (x + y);
    end
end

% Remove padding
mat = mat(1:file_in.height, 1:file_in.width);

% Convert matrix to vector
mat = reshape(mat', [], 1);

% Output file
file_out = file_in;
file_out.data = mat;
fwrite_pgm('3-laplacian.pgm', file_out);