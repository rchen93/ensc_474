% Assignment 5
% ENSC 474 - Spring 2018
% 
% Richard Chen
% ryc5@sfu.ca
% 301171618

%% Setup
clear
clc

% Add path to custom functions
addpath ../lib

%% Question 1
% Perform homomorphic filtering on the input images
c = 2;
do = 200;
gammaH = 2.0;
gammaL = 0.5;

files = ["finger1", "finger2"];

for f = files
    % Open file
    file_in = fread_pgm(f + '.pgm');
    
    % Apply homomorphic filter
    file_out = homomorph(file_in, gammaH, gammaL, do, c);

    % Output file
    fwrite_pgm(f + '_filt.pgm', file_out);
end

%% Question 2
% a. Apply median filtering to the given image and save the restored 
%    image as median.pgm.
% b. Apply adaptive median filtering as described in class and save the 
%    restored image as AdaptiveMedian.pgm.

% Open file
file_in = fread_pgm('NoisyImg.pgm');

% a. Apply median filter of size 3x3
file_out_median = filter_median(file_in, 3);

% b. Apply adaptive median filter
max_window_size = 11;
file_out_adaptive = filter_median_adaptive(file_in, max_window_size);

% Output file
fwrite_pgm('NoisyImg_median.pgm', file_out_median);
fwrite_pgm('NoisyImg_adaptive.pgm', file_out_adaptive);

%% Question 3a
% Apply degradation function to blur original image

% Open file
file_in = fread_pgm('license.pgm');

% Create padding of 2M x 2N
mat = zeros(2 * file_in.height, 2 * file_in.width);

% Convert vector to matrix
mat(1:file_in.height, 1:file_in.width) = reshape(file_in.data, file_in.width, file_in.height)';

% Center the transform
[row, col] = size(mat);
for x = 1:row
    for y = 1:col
        mat(x, y) = mat(x, y) * (-1) ^ (x + y);
    end
end

% 2-D FFT
mat = fft2(mat);

% Create degradation function
k = 0.0025;

degrade = zeros(2 * file_in.height, 2 * file_in.width);
[row, col] = size(mat);
center = [row/2 col/2];

for u = 1:row
    for v = 1:col
        degrade(u, v) = exp(-k * ((u - center(1))^2 + (v - center(2))^2) ^ (5/6));
    end
end

% Apply filter
filtered = mat .* degrade;

% Inverse 2-D FFT
filtered = real(ifft2(filtered));

% Center the transform
[row, col] = size(filtered);
for x = 1:row
    for y = 1:col
        filtered(x, y) = filtered(x, y) * (-1) ^ (x + y);
    end
end

% Remove padding
filtered = filtered(1:file_in.height, 1:file_in.width);

% Convert matrix to vector
filtered = reshape(filtered', [], 1);

% Output file
file_out = file_in;
file_out.data = filtered;
fwrite_pgm('license_blurred.pgm', file_out);

%% Question 3b
% Reverse degradation function to de-blur image

% Open file
file_in = fread_pgm('license_blurred.pgm');

% Create array of low pass filter radii
radius = [0 40 70 85];

for r = radius
    % Create padding of 2M x 2N
    mat = zeros(2 * file_in.height, 2 * file_in.width);

    % Convert vector to matrix
    mat(1:file_in.height, 1:file_in.width) = reshape(file_in.data, file_in.width, file_in.height)';

    % Center the transform
    [row, col] = size(mat);
    for x = 1:row
        for y = 1:col
            mat(x, y) = mat(x, y) * (-1) ^ (x + y);
        end
    end

    % 2-D FFT
    mat = fft2(mat);

    % Create degradation function
    k = 0.0025;

    degrade = zeros(2 * file_in.height, 2 * file_in.width);
    [row, col] = size(mat);
    center = [row/2 col/2];

    for u = 1:row
        for v = 1:col
            degrade(u, v) = exp(-k * ((u - center(1))^2 + (v - center(2))^2) ^ (5/6));
        end
    end
    
    % Apply filter
    filtered = mat ./ degrade;

    % Create low pass filters
    filt_lp = zeros(2 * file_in.height, 2 * file_in.width);
    [row, col] = size(mat);
    center = [row/2 col/2];

    for u = 1:row
        for v = 1:col
            if (center(1) - u)^2 + (center(2) - v)^2 < r^2 || r == 0
                filt_lp(u, v) = 1;
            end
        end
    end
    
    filtered = filtered .* filt_lp;

    % Inverse 2-D FFT
    filtered = real(ifft2(filtered));

    % Center the transform
    [row, col] = size(filtered);
    for x = 1:row
        for y = 1:col
            filtered(x, y) = filtered(x, y) * (-1) ^ (x + y);
        end
    end

    % Remove padding
    filtered = filtered(1:file_in.height, 1:file_in.width);

    % Convert matrix to vector
    filtered = reshape(filtered', [], 1);

    % Output file
    file_out = file_in;
    file_out.data = filtered;
    fwrite_pgm(['license_deblur_r' num2str(r) '.pgm'], file_out);
end