% Assignment 6
% ENSC 474 - Spring 2018
% 
% Richard Chen
% ryc5@sfu.ca
% 301171618

%% Setup
close all
clear
clc

% Add path to custom functions
addpath ../lib

%% Question 1
% Remove motion blur

% Open file
file_in = fread_pgm('MotionBlurred.pgm');

% Convert vector to matrix
mat = reshape(file_in.data, file_in.width, file_in.height)';

% 2-D FFT
mat = pad_spatial(mat);
mat = center_spatial(mat);
mat = fft2(mat);

% Get size of padded matrix
[padded_row, padded_col] = size(mat);

% Create laplacian in frequency domain
laplacian = [0 -1 0; -1 4 -1; 0 -1 0];
laplacian = pad_spatial(laplacian, padded_row, padded_col);
laplacian = center_spatial(laplacian);
laplacian = fft2(laplacian);

% Create restoration function
T = 1;
a = 0.05;
b = 0.05;

gamma = 0.0005;

restore = zeros(padded_row, padded_col);
center = [padded_row/2 padded_col/2];

for u = 1:padded_row
    for v = 1:padded_col
        % Calculate linear motion argument and center transform
        arg = pi * ((u - center(1)) * a + (v - center(2)) * b);
        
        % Handle divide by zero
        if arg == 0
            arg = 0.001;
        end
        
        % Calculate linear motion
        motion = T / arg * sin(arg) * exp(-1j * arg);
        
        mag_sqr = conj(motion) * motion;
        lap_sqr = abs(laplacian(u, v))^2;
        restore(u, v) = (conj(motion) / (mag_sqr + gamma * lap_sqr));
    end
end

% Apply filter
filtered = mat .* restore;

% Inverse 2-D FFT
filtered = real(ifft2(filtered));

% Center the transform
filtered = center_spatial(filtered);

% Remove padding
filtered = filtered(1:file_in.height, 1:file_in.width);

% Convert matrix to vector
filtered = reshape(filtered', [], 1);

% Output file
file_out = file_in;
file_out.data = filtered;
file_out = histogram_equalization(file_out);
fwrite_pgm(['1-unblurred_' num2str(gamma) '.pgm'], file_out);

%% Question 2a
% Implement Marr-Hildreth for edge detection

% Open file
file_in = fread_pgm('Lenna.pgm');

% Create Marr-Hildreth mask
filt = marr_hildreth_filter(3.5);

% Apply filter
file_out = filter_weighted_avg(file_in, filt);

% Edge detection
mat = reshape(file_out.data, file_out.width, file_out.height)';
mat = edge_detection(mat, 15) * 255;
mat = reshape(mat', [], 1);
file_out.data = mat;

% Output file
fwrite_pgm('2a-mh-edge.pgm', file_out);

%% Question 2b
% Canny edge detectors for edge detection

% Open file
file_in = fread_pgm('Lenna.pgm');

% Apply Gaussian filter
gauss = gaussian_filter(0.2);
file_in = filter_weighted_avg(file_in, gauss);

% Create Sobel filter
sobel_x = [-1 0 1; -2 0 2; -1 0 1];
sobel_y = [-1 -2 -1; 0 0 0; 1 2 1];

% Apply filters
g_x = filter_weighted_avg(file_in, sobel_x);
g_y = filter_weighted_avg(file_in, sobel_y);

% Calculate gradient magnitude
m = sqrt(g_x.data.^2 + g_y.data.^2);
m = (m - min(m)) / (max(m) - min(m)) * 255;
m_mat = reshape(m, file_in.width, file_in.height)';

% Calculate gradient angle/direction in degrees
a = rad2deg(atan(g_y.data ./ g_x.data));
a_mat = reshape(a, file_in.width, file_in.height)';

% Non-maximum suppresion
[row, col] = size(a_mat);
edge = zeros(row, col);

for x = 2:row-1
    for y = 2:col-1
        angle = a_mat(x, y);
        
        % 0
        if angle >= -22.5 && angle <= 22.5
            % Compare up down
            if m_mat(x, y) > m_mat(x, y-1)  &&  m_mat(x, y) > m_mat(x, y+1)
                edge(x, y) = m_mat(x, y);
            end
            
        % 45
        elseif angle >= -67.5 && angle <= -22.5
            % Compare diagonals
            if m_mat(x, y) > m_mat(x-1, y+1) && m_mat(x, y) > m_mat(x+1, y-1)
                edge(x, y) = m_mat(x, y);
            end
            
        % -45
        elseif angle <= 67.5 && angle >= 22.5
            % Compare diagonals
            if m_mat(x, y) > m_mat(x-1, y-1) && m_mat(x, y) > m_mat(x+1, y+1)
                edge(x, y) = m_mat(x, y);
            end
            
        % 90
        elseif angle < -67.5 && angle > 67.5     
            % Compare left right
            if m_mat(x, y) > m_mat(x-1, y) && m_mat(x, y) > m_mat(x+1, y)
                edge(x, y) = m_mat(x, y);
            end
        end
    end
end
edge = (edge - min(edge(:))) / (max(edge(:)) - min(edge(:))) * 700;

% Double thresholding with 8-connectivity
thresh_h = 170;
thresh_l = 5;

valid = edge > thresh_h;

for x = 2:row-1
    for y = 2:col-1
        if edge(x, y) > thresh_l
            % Check for 8-connectivity
            kernel = valid(x-1:x+1, y-1:y+1);
            if sum(kernel(:)) > 1
                valid(x, y) = 1;
            end
        end
    end
end
valid = reshape(valid', [], 1) * 255;

% Output file
file_out = file_in;
file_out.data = valid;
fwrite_pgm('2b-canny.pgm', file_out);

%% Question 3
% Hough Transform for circle detection

% Open file
file_in = fread_pgm('CircleSquare.pgm');

% Apply Gaussian filter
gauss = gaussian_filter(0.2);
file_in = filter_weighted_avg(file_in, gauss);

% Create Sobel filter
sobel_x = [-1 0 1; -2 0 2; -1 0 1];
sobel_y = [-1 -2 -1; 0 0 0; 1 2 1];

% Apply filters
g_x = filter_weighted_avg(file_in, sobel_x);
g_y = filter_weighted_avg(file_in, sobel_y);

% Calculate gradient magnitude
m = sqrt(g_x.data.^2 + g_y.data.^2);
m = (m - min(m)) / (max(m) - min(m)) * 255;
m_mat = reshape(m, file_in.width, file_in.height)';

% Calculate gradient angle/direction in degrees
a = rad2deg(atan(g_y.data ./ g_x.data));
a_mat = reshape(a, file_in.width, file_in.height)';

% Non-maximum suppresion
[row, col] = size(a_mat);
edge = zeros(row, col);

for x = 2:row-1
    for y = 2:col-1
        angle = a_mat(x, y);
        
        % 0
        if angle >= -22.5 && angle <= 22.5
            % Compare up down
            if m_mat(x, y) > m_mat(x, y-1)  &&  m_mat(x, y) > m_mat(x, y+1)
                edge(x, y) = m_mat(x, y);
            end
            
        % 45
        elseif angle >= -67.5 && angle <= -22.5
            % Compare diagonals
            if m_mat(x, y) > m_mat(x-1, y+1) && m_mat(x, y) > m_mat(x+1, y-1)
                edge(x, y) = m_mat(x, y);
            end
            
        % -45
        elseif angle <= 67.5 && angle >= 22.5
            % Compare diagonals
            if m_mat(x, y) > m_mat(x-1, y-1) && m_mat(x, y) > m_mat(x+1, y+1)
                edge(x, y) = m_mat(x, y);
            end
            
        % 90
        elseif angle < -67.5 && angle > 67.5     
            % Compare left right
            if m_mat(x, y) > m_mat(x-1, y) && m_mat(x, y) > m_mat(x+1, y)
                edge(x, y) = m_mat(x, y);
            end
        end
    end
end
edge = (edge - min(edge(:))) / (max(edge(:)) - min(edge(:))) * 700;

% Double thresholding with 8-connectivity
thresh_h = 70;
thresh_l = 1;

valid = edge > thresh_h;

for x = 2:row-1
    for y = 2:col-1
        if edge(x, y) > thresh_l
            % Check for 8-connectivity
            kernel = valid(x-1:x+1, y-1:y+1);
            if sum(kernel(:)) > 1
                valid(x, y) = 1;
            end
        end
    end
end

% Detect circles in ab space
radii = 32:65;

accum = zeros(row, col, size(radii, 2));
for r = 1:size(radii, 2)
    for x = 1:row
        for y = 1:col
            if valid(x, y) == 1
                for theta = 1:360
                    a = round(x - radii(r) * cos(deg2rad(theta)));
                    b = round(y - radii(r) * sin(deg2rad(theta)));

                    if a > 0 && a <= row && b > 0 && b <= col
                        accum(a, b, r) = accum(a, b, r) + 1;
                    end
                end
            end
        end
    end
end
accum = (accum - min(accum(:))) / (max(accum(:)) - min(accum(:))) * 255;

% Apply thresholding to center
thresh = 128;

accum = accum > thresh;
valid_center = zeros(size(accum, 1), size(accum, 2));
[row, col] = size(valid_center);

for r = 1:size(accum, 3)
    for a = 1:size(accum, 1)
        for b = 1:size(accum, 2)
            if accum(a, b, r) == 1
                kernel = accum(a-1:a+1, b-1:b+1, r);
                
                if sum(kernel(:)) > 1
                    for theta = 1:360
                        x = round(a + radii(r) * cos(deg2rad(theta)));
                        y = round(b + radii(r) * sin(deg2rad(theta)));

                        if x > 0 && x <= row && y > 0 && y <= col
                            valid_center(x, y) = 1;
                        end
                    end
                end
            end
        end
    end
end

% Overlay on original
mat = reshape(file_in.data, file_in.width, file_in.height)';
for x = 1:size(mat, 1)
    for y = 1:size(mat, 2)
        if valid_center(x, y) == 1
            mat(x, y) = 255;
        end
    end
end

data = reshape(mat', [], 1);

% Output file
file_out = file_in;
file_out.data = data;
fwrite_pgm('3-hough-circle.pgm', file_out);