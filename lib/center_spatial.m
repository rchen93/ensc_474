function mat_out = center_spatial(mat_in)
    %CENTER_SPATIAL centers in spatial domain
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   mat_out = center_spatial(mat_in)
    %
    %   mat_in: is the image matrix of size M x N
    %   mat_out: the image matrix after centering in spatial domain
    
    % Get size of input matrix
    [row, col] = size(mat_in);
    
    % Center the transform in the spatial domain
    mat_out = zeros(row, col);
    for x = 1:row 
        for y = 1:col
            mat_out(x, y) = mat_in(x, y) * (-1) ^ (x + y);
        end
    end
end