function edges = edge_detection(mat, threshold)
    %EDGE_DETECTION returns the zero crossing location 
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   edges = edge_detection(mat, threshold)
    %
    %   See lecture slides for Chapter 10 page 35
    %
    %   To find the zero crossing at location p, the signs of at 
    %   least two of its opposing neighboring pixels must differ.
    %
    %   There are four cases to test: 
    %       - left/right, up/down and the two diagonals.
    %
    %   If the values are being compared against a threshold, the
    %   absolute value of their numerical differences must exceed the
    %   threshold before we can call p a zero crossing.
    
    if ~exist('threshold', 'var')
        threshold = 0;
    end

    % Vertical edges
    % Shift image one pixel to the left and right
    left = circshift(mat, [0, -1]);
    right = circshift(mat, [0, 1]);

    % The vertical edges
    %   a) positive to negative from left
    %   b) negative to positive to right
    %   c) positive to negative or negative to positive from left to right
    vert_a = mat < 0 & left > 0 & abs(left - mat) > threshold;
    vert_b = mat < 0 & right > 0 & abs(right - mat) > threshold;
    vert_c = mat == 0 & sign(left) ~= sign(right) & abs(left - right) > threshold;
    vert_edge = vert_a | vert_b | vert_c; 
    vert_edge(:, [1 end]) = 0;

    % Horizontal edges
    % Shift image one pixel to the top and bottom
    top = circshift(mat, [-1, 0]);
    left = circshift(mat, [1, 0]);

    % The horizontal edges
    %   a) positive to negative from left
    %   b) negative to positive to right
    %   c) positive to negative or negative to positive from left to right
    hor_a = mat < 0 & top > 0 & abs(top - mat) > threshold;
    hor_b = mat < 0 & left > 0 & abs(left - mat) > threshold;
    hor_c = mat == 0 & sign(top) ~= sign(left) & abs(top - left) > threshold;
    hor_edge = hor_a | hor_b | hor_c; 
    hor_edge([1 end], :) = 0;

    % Combine vertical and horizontal edges
    edges = vert_edge | hor_edge;
end