function file_out = filter_median_adaptive(file_obj,max_window,padding)
    %FILTER_MEDIAN_ADAPTIVE filter image using the adaptive median method
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_reduced = filter_median_adaptive(file_obj,padding)
    %
    %   file_obj is a struct with fields
    %   max_window is the maximum window size of the filter
    %   padding is the value used to pad the boundaries
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel
    
    % If not specified, pad with 0's
    if ~exist('padding','var')
        padding = 0.0;
    end
    padding = padding * file_obj.max_value;
        
    % Width of padding
    padding_width = floor(max_window / 2);
    
    % Convert to data from vector to matrix and pad
    data = reshape(file_obj.data, file_obj.width, file_obj.height)';
    data = padarray(data, [padding_width padding_width], padding, 'both');

    % Copy the input file
    file_out = file_obj;
    
    % Image indices
    row_indices = 1 + padding_width : file_obj.height + padding_width;
    col_indices = 1 + padding_width : file_obj.width + padding_width;
    
    % Apply filter
    img_idx = 1;
    for row = row_indices
        for col = col_indices
            current_state = 'a';
            output = nan;
            filter_size_change = 1;
            
            % Start filter size at 3
            filter_size = 3;
            filter_width = floor(filter_size / 2);
            
            while isnan(output)
                % Recalculate image kernel
                if filter_size_change == 1
                    % Get filter kernel
                    kernel_row = row - filter_width : row + filter_width;
                    kernel_col = col - filter_width : col + filter_width;
                    img_kernel = data(kernel_row, kernel_col);

                    % Parameters
                    z_med = median(img_kernel(:));
                    z_min = min(img_kernel(:));
                    z_max = max(img_kernel(:));
                    z_xy = data(row, col);
                    
                    filter_size_change = 0;
                end
                
                % State machine
                switch current_state
                    % Level A
                    case 'a'
                        a1 = z_med - z_min;
                        a2 = z_med - z_max;

                        if a1 > 0  && a2 < 0
                            % Go to Level B
                            current_state = 'b';
                            
                        else
                            % Increase filter size
                            filter_width = filter_width + 1;
                            filter_size = 2 * filter_width + 1;
                            
                            % Request to recalculate kernel
                            filter_size_change = 1;
                            
                            if filter_size < max_window
                                output = z_med;
                            else
                                % Repeat Level A
                            end
                        end
                            
                    % Level B
                    case 'b'
                        b1 = z_xy - z_min;
                        b2 = z_xy - z_max;
                        
                        if b1 > 0 && b2 < 0
                            output = z_xy;
                        else
                            output = z_med;
                        end
                end
            end
            
            % Output data
            file_out.data(img_idx) = output;
            img_idx = img_idx + 1;
        end
    end
end