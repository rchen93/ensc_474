function file_out = filter_weighted_avg(file_obj,filter_cell,padding)
    %FILTER_WEIGHTED_AVG filter image with weighted average
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_reduced = filter_weighted_avg(file_obj,filter_cell)
    %
    %   filter_cell is a weighted NxN matrix
    %   file_obj is a struct with fields
    %   padding is the value used to pad the boundaries
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel
    
    % If not specified, pad with 0's
    if ~exist('padding','var')
        padding = 0.0;
    end
    padding = padding * file_obj.max_value;
    
    % Check that filter_cell is NxN where N is odd
    [r, c] = size(filter_cell);
    assert(r == c, 'Filter cell must be square')
    assert(mod(r, 2) == 1, 'Filter cell must be odd to center around pixel')
    
    % Width of padding
    filter_width = floor(r / 2);
    
    % Convert to data from vector to matrix and pad
    data = reshape(file_obj.data, file_obj.width, file_obj.height)';
    data = padarray(data, [filter_width filter_width], padding, 'both');

    % Copy the input file
    file_out = file_obj;
    
    % Image indices
    row_indices = 1 + filter_width : file_obj.height + filter_width;
    col_indices = 1 + filter_width : file_obj.width + filter_width;
    
    % Apply filter
    img_idx = 1;
    for row = row_indices
        for col = col_indices
            % Get filter kernel
            kernel_row = row - filter_width : row + filter_width;
            kernel_col = col - filter_width : col + filter_width;
            img_kernel = data(kernel_row, kernel_col);
            
            % Apply filter
            img_kernel = img_kernel .* filter_cell;
            img_kernel = sum(img_kernel(:));
            
            % Output data
            file_out.data(img_idx) = img_kernel;
            img_idx = img_idx + 1;
        end
    end
end