function file_obj = fread_pgm(filename)
    %FREAD_PGM Read PGM image file into file object
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_obj = fread_pgm(filename)
    %
    %   Object structure returned contains the following attributes
    %   file_obj struct with fields
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel

    % Get file handle
    file_id = fopen(filename);

    % Read file contents
    file_obj.data = fread(file_id);

    % Delimiters
    % - blank (32 or 0x20)
    % - TABs (9 or 0x09)
    % - CRs (13 or 0x0D)
    % - LFs (10 or 0x0A)
    delimiters = [32, 9, 13, 10];
    
    % Comments '#' (35 or 0x23)
    comment_char = 35;
    
    % Search for header indices
    comment_found = 0;
    start_index = 1;
    header = {};
    for i = 1:length(file_obj.data)
        % Comment character found
        if file_obj.data(i) == comment_char
            comment_found = 1;
            
        elseif comment_found == 1
            % Comment ignored until LF character (10 or 0x0A)
            if file_obj.data(i) == 10
                comment_found = 0;
                start_index = i + 1;
            end
                
        % Found header
        elseif ismember(file_obj.data(i), delimiters)
            str = char(file_obj.data(start_index:i-1)');
            start_index = i + 1;
            header = [header str];
        end
        
        if length(header) == 4
            start_of_data = start_index;
            break
        end
    end
        
    % Edit data to exclude header
    file_obj.data = file_obj.data(start_of_data:end);

    % Add header information as object attributes
    file_obj.magic = header{1};
    file_obj.width = str2double(header{2});
    file_obj.height = str2double(header{3});
    file_obj.max_value = str2double(header{4});
    
    % Close file
    fclose(file_id);
end

