function fwrite_pgm(filename, file_obj)
    %FWRITE_PGM Write PGM image file from file object
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   fwrite_pgm(filename, file_obj)
    %
    %   Object structure must contain the following attributes
    %   file_obj struct with fields
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel

    % Get file handle
    file_id = fopen(filename, 'w+');

    % Convert header back to ASCII decimal
    header = [file_obj.magic ' ' ...
              num2str(file_obj.width) ' ' ... 
              num2str(file_obj.height) ' ' ...
              num2str(file_obj.max_value) 10]';

    header = double(header);

    % Write to file
    fwrite(file_id, [header; file_obj.data]);
    
    % Close file
    fclose(file_id);
end

