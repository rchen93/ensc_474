function file_out = histogram_equalization(file_obj)
    %HISTOGRAM_EQUALIZATION filter image with weighted average
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_reduced = histogram_equalization(file_obj)
    %
    %   file_obj is a struct with fields
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel

    % Copy the input file
    file_out = file_obj;
   
    % Collect histogram data
    histogram = zeros(1, file_obj.max_value + 1);
    
    for i = 1:length(file_obj.data)
        value = file_obj.data(i) + 1;
        
        % Limit to integer values
        value = floor(value);
        
        % Saturate to min and max range of PGM
        value = min(max(value, 1), file_obj.max_value + 1);
        
        histogram(value) = histogram(value) + 1;
    end
    histogram = histogram / length(file_obj.data);
    
    % Calculate transform
    transform = cumsum(histogram) * file_obj.max_value;

    % Map image to new grey levels
    for i = 1:length(file_obj.data)
        value = file_obj.data(i) + 1;
        
        % Limit to integer values
        value = floor(value);
        
        % Saturate to min and max range of PGM
        value = min(max(value, 1), file_obj.max_value + 1);
        
        file_out.data(i) = round(transform(value));
    end
end