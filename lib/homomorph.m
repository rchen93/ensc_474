function file_out = homomorph(file_in, gammaH, gammaL, do, c)
    %HOMOMORPH homomorphic filter to bring out low level details
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_reduced = homomorph(file_in)
    %
    %   file_in is a struct with fields
    %   gammaH is the upper value of the curve
    %   gammaL is the lower value of the curve
    %   do is the radius of the filter
    %   c define the sharpness of the radius
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel

    % Create padding of 2M x 2N
    mat = zeros(2 * file_in.height, 2 * file_in.width);

    % Apply natural log and convert vector to matrix
    mat(1:file_in.height, 1:file_in.width) = reshape(log(file_in.data + 1), file_in.width, file_in.height)';

    % Center the transform
    [row, col] = size(mat);
    for x = 1:row
        for y = 1:col
            mat(x, y) = mat(x, y) * (-1) ^ (x + y);
        end
    end

    % 2-D FFT
    mat = fft2(mat);

    % Create homomorphic filter
    filt = zeros(2 * file_in.height, 2 * file_in.width);
    [row, col] = size(mat);
    center = [row/2 col/2];

    for u = 1:row
        for v = 1:col
            d = ((center(1) - u)^2 + (center(2) - v)^2) ^ 0.5;
            filt(u, v) = (gammaH - gammaL) * (1 - exp(-c * d^2 / do^2)) + gammaL;
        end
    end

    % Apply filter
    filtered = mat .* filt;

    % Inverse 2-D FFT
    filtered = real(ifft2(filtered));

    % Center the transform
    [row, col] = size(filtered);
    for x = 1:row
        for y = 1:col
            filtered(x, y) = filtered(x, y) * (-1) ^ (x + y);
        end
    end

    % Remove padding
    filtered = filtered(1:file_in.height, 1:file_in.width);

    % Apply exponential
    filtered = exp(filtered);

    % Convert matrix to vector
    filtered = reshape(filtered', [], 1);

    % Histogram equalization
    file_out = file_in;
    file_out.data = filtered;
    file_out = histogram_equalization(file_out);
end