function filter = marr_hildreth_filter(sigma)
    %MARR_HILDRETH_FILTER create a marr_hildreth filter mask
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   filter = marr_hildreth_filter(sigma)
    %
    %   The Marr-Hildreth mask is created using the Laplacian of Gaussian.
    %   Since 99.7% of the volume under the 2D Gaussian surface lies
    %   between +/- 3 sigma, we need a mask size greater than or equal to 6
    %   sigmas. However, in order to create a mask, the size must be an odd
    %   number. (See lecture slides for Chapter 10 page 34)
    
    % Calculate size needed for mask
    filter_size = 6 * ceil(sigma);
    
    % Get the mask size on either side of centered pixel
    % Use floor to get the smallest odd numbered mask
    filter_size = floor(filter_size / 2);
    
    % Create mesh grid
    [x, y] = meshgrid(-filter_size:filter_size, -filter_size:filter_size);
    
    % Calculate the Laplacian of Gaussian
    % The equation used is:
    %  (x^2 + y^2 + 2*sigma^2) / sigma^4 * exp(-(x^2 + y^2) / (2*sigma^2))
    coeff = (x.^2 + y.^2 - 2*sigma^2) / sigma^4;
    expon = exp( -(x.^2 + y.^2) / (2*sigma^2));
    filter = coeff .* expon;
end