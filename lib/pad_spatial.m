function mat_out = pad_spatial(mat_in, M_out, N_out)
    %PAD_SPATIAL pads in prepare for FFT
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   mat_out = pad_spatial(mat_in, M_out, N_out)
    %
    %   mat_in: is the image matrix of size M x N
    %   mat_out: padded matrix of size 2M x 2N if not specified, 
    %            and M_out, N_out if specified
    
    % Get size of input matrix
    [row, col] = size(mat_in);
    
    % If not specified, use twice the input size
    if ~exist('M_out','var')
        M_out = 2 * row;
    end
    
    if ~exist('N_out','var')
        N_out = 2 * col;
    end
    
    % Pad with zeros
    mat_out = zeros(M_out, N_out);
    
    % Add input matrix to top left corner of padded matrix
    mat_out(1:row, 1:col) = mat_in;
end