function file_reduced = reduce_avg(file_obj,factor)
    %REDUCE_DEL Reduce PGM image by integer factor using average method
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_reduced = reduce_avg(file_obj,factor)
    %
    %   factor is an integer factor to reduce by
    %   file_obj is a struct with fields
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel
    
    % Check that factor provided is an integer
    assert(floor(factor) == factor, 'factor must be an integer')

    % Reduce the size by discarding
    file_reduced = file_obj;
    file_reduced.width = floor(file_obj.width / factor);
    file_reduced.height = floor(file_obj.height / factor);
    file_reduced.data = zeros(file_reduced.height, file_reduced.width);

    for row = 1:file_obj.height
        for col = 1:file_obj.width
            index = (row - 1) * file_obj.width + col;

            % Reduced index
            reduced_row = floor((row - 1) / factor) + 1;
            reduced_col = floor((col - 1) / factor) + 1;
            
            if reduced_row <= file_reduced.height && reduced_col <= file_reduced.width
                % Sum into the reduced index
                file_reduced.data(reduced_row, reduced_col) = file_reduced.data(reduced_row, reduced_col) + file_obj.data(index);
            end
        end
    end
    
    % Average the summed elements
    file_reduced.data = file_reduced.data / factor / factor;
    
    % Reshape into column vector
    file_reduced.data = reshape(file_reduced.data', [], 1);
    
    % Sanity check for the algorithm
    assert(file_reduced.width * file_reduced.height == length(file_reduced.data), 'algorithm is not working correctly')
end

