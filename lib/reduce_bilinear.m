function file_reduced = reduce_bilinear(file_obj,factor)
    %REDUCE_DEL Reduce PGM image by any factor using bilinear interpolation
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_reduced = reduce_bilinear(file_obj,factor)
    %
    %   factor is the factor to reduce by
    %   file_obj is a struct with fields
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel
    
    file_reduced = file_obj;
    file_reduced.width = floor((file_obj.width - 1) / factor + 1);
    file_reduced.height = floor((file_obj.height - 1) / factor + 1);
    file_reduced.data = zeros(file_reduced.width * file_reduced.height, 1);
    
    % Calculate new column indices
    col_indices = [1, zeros(1, file_reduced.width - 1)];
    
    for i = 2:file_reduced.width
        col_indices(i) = col_indices(i-1) + factor;
    end
    
    % Calculate new row indices
    row_indices = [1, zeros(1, file_reduced.height - 1)];
    
    for i = 2:file_reduced.height
        row_indices(i) = row_indices(i-1) + factor;
    end
    
    % Bilinear interpolation
    i = 1;
    for row = row_indices
        for col = col_indices
            % Calculate coordinates
            x1 = floor(col);
            x2 = ceil(col);
            y1 = ceil(row);
            y2 = floor(row);

            % Get indices
            q11_index = (y1 - 1) * file_obj.width + x1;
            q12_index = (y2 - 1) * file_obj.width + x1;
            q21_index = (y1 - 1) * file_obj.width + x2;
            q22_index = (y2 - 1) * file_obj.width + x2;

            % Get the four values
            q11 = file_obj.data(q11_index);
            q12 = file_obj.data(q12_index);
            q21 = file_obj.data(q21_index);
            q22 = file_obj.data(q22_index);

            % Calculate the bilinear interpolation
            if x1 ~= x2 && y1 ~= y2
                r1 = (col - x1) / (x2 - x1) * (q21 - q11) + q11;
                r2 = (col - x1) / (x2 - x1) * (q22 - q12) + q12;
                p = (row - y1) / (y2 - y1) * (r2 - r1) + r1;

            elseif x1 ~= x2 && y1 == y2
                p = (col - x1) / (x2 - x1) * (q22 - q12) + q12;

            elseif x1 == x2 && y1 ~= y2
                p = (row - y1) / (y2 - y1) * (q12 - q11) + q11;

            elseif x1 == x2 && y1 == y2
                p = q11;

            end
            
            file_reduced.data(i) = p;
            i = i + 1;
        end
    end
    
    % Sanity check for the algorithm
    assert(file_reduced.width * file_reduced.height == length(file_reduced.data), 'algorithm is not working correctly')
end

