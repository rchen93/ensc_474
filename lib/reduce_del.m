function file_reduced = reduce_del(file_obj,factor)
    %REDUCE_DEL Reduce PGM image by integer factor using delete method
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_reduced = reduced_del(file_obj,factor)
    %
    %   factor is an integer factor to reduce by
    %   file_obj is a struct with fields
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel
    
    % Check that factor provided is an integer
    assert(floor(factor) == factor, 'factor must be an integer')

    % Reduce the size by discarding
    file_reduced = file_obj;
    file_reduced.data = [];
    file_reduced.width = floor(file_obj.width / factor);
    file_reduced.height = floor(file_obj.height / factor);

    for row = 1:file_obj.height
        for col = 1:file_obj.width
            index = (row - 1) * file_obj.width + col;

            if mod(row, factor) == 0 && mod(col, factor) == 0
                file_reduced.data = [file_reduced.data; file_obj.data(index)];
            end
        end
    end
    
    % Sanity check for the algorithm
    assert(file_reduced.width * file_reduced.height == length(file_reduced.data), 'algorithm is not working correctly')
end

