function file_reduced = reduce_grey_levels(file_obj,factor,gradient)
    %REDUCE_DEL Reduce PGM grey levels by integer factor
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_reduced = reduce_grey_levels(file_obj,factor)
    %
    %   factor is an integer factor to reduce by
    %   gradient is the point between floor and ceil to choose
    %   file_obj is a struct with fields
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel
    
    % Default gradient to ceiling
    if ~exist('gradient','var')
        gradient = 1.0;
    end
    
    % Check that factor provided is an integer
    assert(floor(factor) == factor, 'factor must be an integer')

    % Reduce the size by discarding
    file_reduced = file_obj;

    % Reduce the grey levels by the specified factor
    file_reduced.data = floor((file_reduced.data - 1) / factor) * factor;
    
    % Apply gradient percentage
    file_reduced.data = file_reduced.data + factor * gradient;
end

