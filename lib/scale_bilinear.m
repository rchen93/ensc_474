function file_out = scale_bilinear(file_obj,factor)
    %SCALE_BILINEAR Scale PGM image by any factor using bilinear interpolation
    %   @class: ENSC 474
    %   @school: Simon Fraser University
    %   @author: ryc5@sfu.ca
    %
    %   file_out = scale_bilinear(file_obj,factor)
    %
    %   factor is the factor to reduce by
    %   file_obj is a struct with fields
    %
    %        data: [(width x height) �1 double]
    %       magic: 'P5'
    %       width: width of image
    %      height: height of image
    %   max_value: max grey value per pixel
    
    file_out = file_obj;
    file_out.width = floor(file_obj.width * factor);
    file_out.height = floor(file_obj.height * factor);
    file_out.data = zeros(file_out.width * file_out.height, 1);
        
    % Calculate new column indices
    step = (file_obj.width - 1) / file_out.width;
    col_indices = [1, zeros(1, file_out.width - 1)];
    for i = 2:file_out.width
        col_indices(i) = col_indices(i-1) + step;
    end
    
    % Calculate new row indices
    step = (file_obj.height - 1) / file_out.height;
    row_indices = [1, zeros(1, file_out.height - 1)];
    for i = 2:file_out.height
        row_indices(i) = row_indices(i-1) + step;
    end
    
    % Bilinear interpolation
    i = 1;
    for row = row_indices
        for col = col_indices
            % Calculate coordinates
            x1 = floor(col);
            x2 = ceil(col);
            y1 = ceil(row);
            y2 = floor(row);

            % Get indices
            q11_index = (y1 - 1) * file_obj.width + x1;
            q12_index = (y2 - 1) * file_obj.width + x1;
            q21_index = (y1 - 1) * file_obj.width + x2;
            q22_index = (y2 - 1) * file_obj.width + x2;

            % Get the four values
            q11 = file_obj.data(q11_index);
            q12 = file_obj.data(q12_index);
            q21 = file_obj.data(q21_index);
            q22 = file_obj.data(q22_index);

            % Calculate the bilinear interpolation
            if x1 ~= x2 && y1 ~= y2
                r1 = (col - x1) / (x2 - x1) * (q21 - q11) + q11;
                r2 = (col - x1) / (x2 - x1) * (q22 - q12) + q12;
                p = (row - y1) / (y2 - y1) * (r2 - r1) + r1;

            elseif x1 ~= x2 && y1 == y2
                p = (col - x1) / (x2 - x1) * (q22 - q12) + q12;

            elseif x1 == x2 && y1 ~= y2
                p = (row - y1) / (y2 - y1) * (q12 - q11) + q11;

            elseif x1 == x2 && y1 == y2
                p = q11;

            end
            
            file_out.data(i) = p;
            i = i + 1;
        end
    end
    
    % Sanity check for the algorithm
    assert(file_out.width * file_out.height == length(file_out.data), 'algorithm is not working correctly')
end